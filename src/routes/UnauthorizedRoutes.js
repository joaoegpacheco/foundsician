import { Result, Button, Layout } from "antd";
import React from "react";
import { withRouter, Route, Switch } from "react-router-dom";

import TopBar from "../components/Menu/TopBar";
import Login from "../pages/Login";
import RegisterUser from "../pages/RegisterUser/index";

const UnauthorizedRoutes = ({ history }) => {
  return (
    <>
      <Layout style={{ minHeight: "100vh" }}>
        <div>
          <TopBar />
          <Switch>
            <Route exact path="/">
              <Login />
            </Route>
            <Route exact path="/login">
              <Login />
            </Route>
            <Route exact path="/register">
              <RegisterUser history={history} />
            </Route>
            <Route path="/*">
              <Result
                status="403"
                title="403"
                subTitle="Desculpe, você não tem permissão para acessar esta página"
                style={{ marginTop: "150px" }}
                extra={
                  <Button
                    type="primary"
                    onClick={() => {
                      history.push("/login");
                    }}
                  >
                    Voltar para Login
                  </Button>
                }
              />
            </Route>
          </Switch>
        </div>
      </Layout>
    </>
  );
};

export default withRouter(UnauthorizedRoutes);
