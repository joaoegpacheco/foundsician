import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { requestLogin } from "../actions/actionLogin";
import AuthenticatedRoutes from "./AuthenticatedRoutes";
import UnauthorizedRoutes from "./UnauthorizedRoutes";

class Routes extends React.Component {
  render() {
    if (!this.props.authentication) {
      return <UnauthorizedRoutes />;
    }
    if (this.props.authentication) {
      return <AuthenticatedRoutes />;
    }
  }
}

const mapStateToProps = (state) => ({
  authentication: state.login.authentication,
});

const mapDispatchToProps = (dispatch) => ({
  authenticate: (user, password) => dispatch(requestLogin(user, password)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Routes));
