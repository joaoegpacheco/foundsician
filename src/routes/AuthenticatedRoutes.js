import { Result, Button, Layout } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Route, Switch } from "react-router-dom";

import { getBookmarks, getUsers } from "../actions/actionBookmarks";
import SliderBar from "../components/Menu/SliderBar";
import CardsFavoritos from "../pages/Bookmarks";
import MusicianProfile from "../pages/MusicianProfile";
import SearchPage from "../pages/SearchPage";
import CurrentUserProfile from "../pages/UserProfile/CurrentUserProfile";
import EditProfile from "../pages/UserProfile/EditProfile";

class AuthenticatedRoutes extends Component {
  // Buscando e guardando os favoritos do usuário
  componentDidMount = () => {
    const { authentication, getBookmarks, getUsers } = this.props;
    fetch(`https://api-scherer.herokuapp.com/book_marks`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authentication,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        getBookmarks(data);
      })
      .catch((error) => {
        return error;
      });

    // Busca todos os usuários cadastrados e guarda no Redux
    fetch(`https://api-scherer.herokuapp.com/users`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authentication,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        getUsers(data);
      })
      .catch((error) => {
        console.error("Error:", error);
        return error;
      });
  };

  render() {
    return (
      <>
        <Layout style={{ minHeight: "100vh" }}>
          <SliderBar />
          <Switch>
            <Route exact path="/">
              <SearchPage />
            </Route>
            <Route exact path="/favoritos">
              <CardsFavoritos />
            </Route>
            <Route exact path="/profile">
              <CurrentUserProfile />
            </Route>
            <Route exact path="/profile/:id">
              <MusicianProfile />
            </Route>
            <Route exact path="/editProfile">
              <EditProfile />
            </Route>
            <Route exact path="/search">
              <SearchPage />
            </Route>
            <Route path="/*">
              <Result
                status="404"
                title="404"
                subTitle="Desculpe, esta página não existe"
                style={{ marginTop: "150px" }}
                extra={
                  <Button
                    type="primary"
                    onClick={() => {
                      this.props.history.push("/profile");
                    }}
                  >
                    Voltar para o profile
                  </Button>
                }
              />
            </Route>
          </Switch>
        </Layout>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.login.user,
  authentication: state.login.authentication,
  bookmarks: state.bookmarks.bookmarks,
});

const mapDispatchToProps = (dispatch) => ({
  getBookmarks: (bookmarks) => dispatch(getBookmarks(bookmarks)),
  getUsers: (users) => dispatch(getUsers(users)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthenticatedRoutes);
