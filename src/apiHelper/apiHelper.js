const apiHelper = (path, method, data, auth) => {
  const params = {
    method,
    headers: {
      "Content-Type": "application/json",
      Authorization: auth,
    },
  };

  if (method !== "GET") {
    params.body = JSON.stringify(data);
  }

  return fetch(`https://api-scherer.herokuapp.com${path}`, params).then((res) => {
    return res;
  });
};

export default apiHelper;
