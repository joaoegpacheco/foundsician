import React from "react";
import renderer from "react-test-renderer";

import Profile from "../components/Profile";

test("Test Profile", () => {
  const component = renderer.create(
    <Profile
      name="Usuário Teste"
      profile="Guitarrista"
      imageUrl="https://www.img-url.com/"
      about="Sobre mim"
      facebookUrl="https://www.facebook.com"
      instagramUrl="https://www.instagram.com"
    />
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
