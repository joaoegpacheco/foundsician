export const profiles = {
  1: "Guitarrista",
  2: "Baixista",
  3: "Baterista",
  4: "Violonista",
  5: "Pianista",
  6: "Saxofonista",
  7: "Vocalista",
  8: "DJ",
  9: "Banda",
};

export const cities = [
  { value: "São Paulo" },
  { value: "Rio de Janeiro" },
  { value: "Curitiba" },
  { value: "Florianópolis" },
  { value: "Porto Alegre" },
  { value: "Belo Horizonte" },
];
