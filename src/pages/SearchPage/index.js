import { SearchOutlined } from "@ant-design/icons";
import { Input, Button, Select } from "antd";
import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

import { addSearch } from "../../actions/actionSearch";
import apiHelper from "../../apiHelper/apiHelper";
import CardList from "../../components/CardList";
import { profiles, cities } from "../../data";

const { Option } = Select;

class SearchPage extends React.Component {
  state = {
    profile: 0,
    city: "",
  };

  setField = (field, value) => {
    this.setState({ [field]: value });
  };

  handleSearch = () => {
    const { authentication, addSearch } = this.props;
    const { profile, city } = this.state;
    apiHelper("/users", "GET", null, authentication)
      .then((res) => res.json())
      .then((data) => {
        console.log("estamos aqui", profile);
        // console.log(profile);
        const search = data.filter(
          (user) => profiles[user.profile] === profile && user.address === city
        );
        addSearch(search);
      });
  };

  render() {
    return (
      <>
        <div>
          <Input.Group compact>
            <Select
              defaultValue="Selecione a Cidade"
              onChange={(value) => this.setField("city", value)}
              style={{ width: 180 }}
            >
              {cities.map((city, key) => (
                <Option value={city.value} key={key}>
                  {city.value}
                </Option>
              ))}
            </Select>
            <Select
              defaultValue="Selecione a Categoria Desejada"
              onChange={(value) => this.setField("profile", value)}
              style={{ width: 250 }}
            >
              {Object.entries(profiles).map((profile, key) => (
                <Option value={profile[1]} key={key}>
                  {profile[1]}
                </Option>
              ))}
            </Select>
            <Button
              type="primary"
              icon={<SearchOutlined />}
              onClick={this.handleSearch}
            >
              Buscar
            </Button>
          </Input.Group>
          <MainContainer>
          {this.props.search !== "" && (
            <CardList list={this.props.search} />
          )}
          </MainContainer>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  search: state.search.resultSearch,
  authentication: state.login.authentication,
});

const mapDispatchToProps = (dispatch) => ({
  addSearch: (search) => dispatch(addSearch(search)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SearchPage));

const MainContainer = styled.div`
width: 1250px; 
height: 100%;
display: flex;
flex-wrap: wrap;
padding-bottom: 40px;
`