import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import "antd/dist/antd.css";
import "../../styles/UserProfile/UserProfile.css";
import { getBookmarks, getUsers } from "../../actions/actionBookmarks";
import Profile from "../../components/Profile";
import { profiles } from "../../data";
import apiHelper from "../../apiHelper/apiHelper";

class MusicianProfile extends React.Component {
  state = {
    musician: "",
  };

  profileChange = (profileId) => {
    const profile = profiles.filter((profile) => profile.id === profileId);
    return profile[0].value;
  };
  componentDidMount = () => {
    const { authentication } = this.props;
    const { id } = this.props.match.params;
    apiHelper(`/users/${id}`, "GET", null, authentication)
      .then((response) => response.json())
      .then((data) => {
        this.setState({ musician: data });
      })
      .catch((error) => {
        console.error("Error:", error);
        return error;
      });
  };

  render() {
    return <Profile {...this.state.musician} />;
  }
}
const mapStateToProps = (state) => ({
  user: state.login.user,
  authentication: state.login.authentication,
  bookmarks: state.bookmarks.bookmarks,
});

const mapDispatchToProps = (dispatch) => ({
  getBookmarks: (bookmarks) => dispatch(getBookmarks(bookmarks)),
  getUsers: (users) => dispatch(getUsers(users)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(MusicianProfile));
