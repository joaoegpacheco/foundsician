import React from "react";
import "antd/dist/antd.css";
import "../../styles/Login/Login.css";
import { Form, Input, Button } from "antd";
import {
  UserOutlined,
  LockOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
} from "@ant-design/icons";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import styled from "styled-components";

import { requestLogin } from "../../actions/actionLogin";
import { errorRules } from "./validator";

const { Password } = Input;

interface LoginStateTypes {
  user: string;
  password: string;
  isLoading: boolean;
}

type FieldSetter = (
  e: React.ChangeEvent<HTMLInputElement>,
  field: "user" | "password"
) => void;

class Login extends React.Component<any, LoginStateTypes> {
  state = {
    user: "",
    password: "",
    isLoading: false,
  };

  setLoading = (isLoading: boolean) => {
    this.setState({ ...this.state, isLoading });
  };

  setField: FieldSetter = (e, field) => {
    this.setState({ ...this.state, [field]: e.target.value });
  };

  handleClick = () => {
    const { user, password } = this.state;
    const { authentication, history } = this.props;

    authentication(user, password, history, this.setLoading);
  };

  showPassword = (visible: boolean) => {
    return visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />;
  };

  render() {
    return (
      <ClassNameLogin>
        <LoginForm>
          <Form name="normal_login" className="login-form">
            <Form.Item label="E-mail" name="username" rules={errorRules.user}>
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Username"
                value={this.state.user}
                onChange={(e) => this.setField(e, "user")}
              />
            </Form.Item>
            <Form.Item
              label="Senha"
              name="password"
              rules={errorRules.password}
            >
              <Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                iconRender={this.showPassword}
                type="password"
                placeholder="Password"
                value={this.state.password}
                onChange={(e) => this.setField(e, "password")}
              />
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                onClick={() => this.handleClick()}
                loading={this.state.isLoading}
              >
                Log in
              </Button>
              <br />
              <Link to="/register">Cadastre-se </Link>
            </Form.Item>
          </Form>
        </LoginForm>
      </ClassNameLogin>
    );
  }
}

const mapStateToProps = (state: any) => ({
  user: state.login.user,
  authentication: state.login.authentication,
});

const mapDispatchToProps = (dispatch: any) => ({
  authentication: (
    user: string,
    password: string,
    history: any,
    setLoading: boolean
  ) => dispatch(requestLogin(user, password, history, setLoading)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));

const ClassNameLogin = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-family: "Sen", sans-serif;
`;

const LoginForm = styled.div`
  width: 350px;
`;
