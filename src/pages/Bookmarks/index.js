import React, { Component } from "react";
import { connect } from "react-redux";

import CardList from "../../components/CardList";

class CardsFavoritos extends Component {
  render() {
    const { bookmarks } = this.props;

    return (
          <CardList list={bookmarks} />
      );
    }
  }

const mapStateToProps = (state) => ({
  user: state.login.user,
  authentication: state.login.authentication,
  bookmarks: state.bookmarks.bookmarks.map(bookmark => bookmark.marked_user)
});
export default connect(mapStateToProps, null)(CardsFavoritos);