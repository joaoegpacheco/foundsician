import React from "react";
import { connect } from "react-redux";

import "antd/dist/antd.css";
import "../../styles/UserProfile/UserProfile.css";

import { Layout, PageHeader, Typography, Row } from "antd";

import { getBookmarks, getUsers } from "../../actions/actionBookmarks";
import facebook from "../../assets/img/facebook.png";
import instagram from "../../assets/img/instagram.png";
import { profiles } from "../../data";

const { Content } = Layout;
const { Paragraph } = Typography;

const IconLink = ({ src, text, href }) => (
  <a className="example-link" href={href} target="_blank">
    <img className="example-link-icon" src={src} alt={text} />
  </a>
);

const ContentLayout = ({ children, extraContent }) => {
  return (
    <Row>
      <div style={{ flex: 1 }}>{children}</div>
      <div className="image">{extraContent}</div>
    </Row>
  );
};

class UserProfile extends React.Component {
  // Buscando e guardando os favoritos do usuário
  componentDidMount = () => {
    const { authentication, getBookmarks, getUsers } = this.props;
    fetch(`https://api-scherer.herokuapp.com/book_marks`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authentication,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        getBookmarks(data);
      })
      .catch((error) => {
        return error;
      });

    // Busca todos os usuários cadastrados e guarda no Redux
    fetch(`https://api-scherer.herokuapp.com/users`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authentication,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        getUsers(data);
      })
      .catch((error) => {
        console.error("Error:", error);
        return error;
      });
  };

  profileChange = (profileId) => {
    const profile = profiles.filter((profile) => profile.id === profileId);
    return profile[0].value;
  };

  render() {
    const {
      name,
      profile,
      facebookUrl,
      instagramUrl,
      about,
      image_url,
    } = this.props.user;

    return (
      <>
        <Layout style={{ minHeight: "100vh" }}>
          <Layout className="site-layout">
            <Content style={{ margin: "0 16px" }}>
              <PageHeader
                title={name}
                className="site-page-header"
                subTitle={this.profileChange(profile)}
                avatar={{ src: image_url }}
              >
                <Paragraph>{about}</Paragraph>
                <div>
                  <IconLink
                    src={facebook}
                    style={{ width: 25 }}
                    href={facebookUrl}
                  />
                  <IconLink
                    src={instagram}
                    style={{ width: 25 }}
                    href={instagramUrl}
                  />
                </div>
              </PageHeader>
            </Content>
          </Layout>
        </Layout>
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.login.user,
  authentication: state.login.authentication,
  bookmarks: state.bookmarks.bookmarks,
});

const mapDispatchToProps = (dispatch) => ({
  getBookmarks: (bookmarks) => dispatch(getBookmarks(bookmarks)),
  getUsers: (users) => dispatch(getUsers(users)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
