import React from "react";
import { connect } from "react-redux";

import "antd/dist/antd.css";
import "../../styles/UserProfile/UserProfile.css";
import { getBookmarks, getUsers } from "../../actions/actionBookmarks";
import Profile from "../../components/Profile";

class CurrentUserProfile extends React.Component {
  render() {
    const {
      name,
      profile,
      facebook_url,
      instagram_url,
      about,
      image_url,
    } = this.props.user;

    const instagramUrl = instagram_url;
    const facebookUrl = facebook_url;
    const imageUrl = image_url;

    return (
      <Profile
        name={name}
        profile={profile}
        imageUrl={imageUrl}
        about={about}
        facebookUrl={facebookUrl}
        instagramUrl={instagramUrl}
      />
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.login.user,
  authentication: state.login.authentication,
  bookmarks: state.bookmarks.bookmarks,
});

const mapDispatchToProps = (dispatch) => ({
  getBookmarks: (bookmarks) => dispatch(getBookmarks(bookmarks)),
  getUsers: (users) => dispatch(getUsers(users)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CurrentUserProfile);
