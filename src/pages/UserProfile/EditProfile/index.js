import React from "react";
import "antd/dist/antd.css";
import "../../../styles/UserProfile/UserProfile.css";
import { notification } from "antd";
import { connect } from "react-redux";

import { addUser } from "../../../actions/actionLogin";
import apiHelper from "../../../apiHelper/apiHelper";
import UserForm from "../../../components/UserForm";

const EditProfile = ({ user, authentication, addUser }) => {
  const userId = user.id;
  const path = `/users/${userId}`;

  const onFinish = ({
    profile,
    cidade,
    name,
    about,
    imageUrl,
    facebookUrl,
    instagramUrl,
    password,
    cellphone,
  }) => {
    const userId = user.id;
    const data = {
      user: {
        name,
        about,
        image_url: imageUrl,
        facebook_url: facebookUrl,
        instagram_url: instagramUrl,
        password,
        password_confirmation: password,
        profile,
        address: cidade,
        cellphone,
      },
    };
    apiHelper(path, "PUT", data, authentication).then((res) => {
      if (res.ok) {
        addUser({ id: userId, ...data.user });
        return notification.success(
          {
            message: "Cadastro atualizado",
          },
          3
        );
      }
    });
  };

  return <UserForm isEdit {...user} onFinish={onFinish} />;
};
const mapStateToProps = (state) => ({
  authentication: state.login.authentication,
  user: state.login.user,
});

const mapDispatchToProps = (dispatch) => {
  return {
    addUser: (user) => dispatch(addUser(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
