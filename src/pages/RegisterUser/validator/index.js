export const errorRules = {
  user: [{ required: true, message: "Por favor, digite seu usuário!" }],
  password: [{ required: true, message: "Por favor, digite sua senha." }],
  name: [
    { required: true, message: "Por favor, preencha com seu nome completo!" },
  ],
  cidade: [{ required: true, message: "Por favor, preencha com sua cidade!" }],
  estado: [{ required: true, message: "Por favor, preencha com seu estado!" }],
  email: [
    { type: "email", message: "Este e-mail não é válido!" },
    { required: true, message: "Por favor, preencha com seu e-mail!" },
  ],
  registerpassword: [{ required: true, message: "Por favor, crie sua senha!" }],
  passwordconfirm: [
    { required: true, message: "Por favor, confirme sua senha!" },
    ({ getFieldValue }) => ({
      validator(rule, value) {
        if (!value || getFieldValue("password") === value) {
          return Promise.resolve();
        }
        return Promise.reject("As senhas não são iguais!");
      },
    }),
  ],
  profile: [{ required: true, message: "Por favor, selecione uma opção!" }],
  cellphone: [
    {
      required: true,
      message: "Por favor, preencha com seu número de telefone!",
    },
  ],
};
