import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { requestRegister } from "../../actions/actionRegister";

import UserForm from "../../components/UserForm";

const RegisterUser = () => {
  const history = useHistory();
  const onFinish = (values) => {
    setLoading(true);
    requestRegister(values)
      .then((data) => {
        console.log("Success:", data);
        setLoading(false);
        history.push("/login");
      })
      .catch((error) => {
        setLoading(false);

        console.error("Error:", error);
        return error;
      });
  };

  const [loading, setLoading] = useState(false);

  return <UserForm onFinish={onFinish} loading={loading} /> };

export default RegisterUser;