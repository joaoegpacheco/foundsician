import { LOGIN, ADD_USER, LOGOUT } from "../actions/actionTypes"

const defaultLoginState = { authentication: null, user: "" };

const login = (state = defaultLoginState, action) => {
  switch (action.type) {
    case LOGIN:
      const { authentication } = action;
      return { ...state, authentication };

    case ADD_USER:
      const { user } = action;
      return { ...state, user };

    case LOGOUT:
      return { ...state ,authentication: false }

    default:
      return state;
  }
};

export default login;
