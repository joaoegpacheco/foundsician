import { ADD_SEARCH } from "../actions/actionTypes"

const defaultSearchState = { resultSearch: "" };

const search = (state = defaultSearchState, action) => {
  switch (action.type) {
    case ADD_SEARCH:
      const { search } = action;
      return { ...state, resultSearch: search };

    default:
      return state;
  }
};

export default search;
