import {
  GET_BOOKMARKS,
  GET_USERS,
  ADD_BOOKMARK,
  DELETE_BOOKMARK,
} from "../actions/actionTypes";

const defaultState = { bookmarks: [], users: "" };

const bookmarks = (state = defaultState, action) => {
  switch (action.type) {
    case GET_BOOKMARKS:
      const { bookmarks } = action;

      return { ...state, bookmarks };

    case GET_USERS:
      const { users } = action;

      return { ...state, users };

    case ADD_BOOKMARK:
      const { bookmark } = action;

      return { ...state, bookmarks: [...state.bookmarks, bookmark] };

    case DELETE_BOOKMARK:
      const { user } = action;

      const filterBookmarks = state.bookmarks.filter(
        (e) => e.marked_user.id !== user.marked_user.id
      );
      return { ...state, bookmarks: [...filterBookmarks] };

    default:
      return state;
  }
};

export default bookmarks;
