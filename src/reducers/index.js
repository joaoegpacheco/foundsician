import { combineReducers } from "redux";

import bookmarksReducer from "./bookmarksReducer";
import loginReducer from "./loginReducer";
import searchReducer from "./searchReducer";

export default combineReducers({
  login: loginReducer,
  search: searchReducer,
  bookmarks: bookmarksReducer,
});
