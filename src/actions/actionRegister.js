import apiHelper from "../apiHelper/apiHelper";

export const requestRegister = (data) => {
  const { name, email, cellphone, password, password_confirmation, profile } = data;
  console.log(data);
  return apiHelper("/users", "POST", {
    user: {
      name,
      user: data.email,
      email,
      cellphone,
      address: data.cidade,
      password,
      password_confirmation,
      profile,
    },
  }).then((response) => response.json());
};
