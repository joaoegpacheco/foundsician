export const LOGIN = "LOGIN";
export const ADD_USER = "ADD_USER";
export const LOGOUT = "LOGOUT";
export const ADD_SEARCH = "ADD_SEARCH";
export const GET_BOOKMARKS = "GET_BOOKMARKS";
export const GET_USERS = "GET_USERS";
export const ADD_BOOKMARK = "ADD_BOOKMARK";
export const DELETE_BOOKMARK = "DELETE_BOOKMARK";

export interface LoginAction {
  type: typeof LOGIN;
  authentication: string;
}

export interface LogoutAction {
  type: typeof LOGOUT;
  authentication: boolean;
}

export interface AddUserAction {
  type: typeof ADD_USER;
  user: string;
}

export type LoginActionTypes = LoginAction | LogoutAction | AddUserAction;

export interface LoginStoreType {
  authentication: string | null;
  user: string;
}

export interface GetBookmarksAction {
  type: typeof GET_BOOKMARKS;
  bookmarks: any;
}

export interface GetUsersAction {
  type: typeof GET_USERS;
  users: string;
}

export interface AddBookmarkAction {
  type: typeof ADD_BOOKMARK;
  bookmark: any;
}

export interface DeleteBookmarkAction {
  type: typeof DELETE_BOOKMARK;
  user: string;
}

export type BookmarksActionTypes = GetBookmarksAction | GetUsersAction | AddBookmarkAction | DeleteBookmarkAction;

export interface BookmarksStoreType {
  bookmarks: any;
  users: string;
}

export interface AddSearchAction {
  type: typeof ADD_SEARCH;
  search: any;
}