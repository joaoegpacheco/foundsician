import { notification } from "antd";

import apiHelper from "../apiHelper/apiHelper";
import {
  LOGIN,
  ADD_USER,
  LOGOUT,
  LoginAction,
  LogoutAction,
  AddUserAction,
} from "./actionTypes";

export const login = (authentication: string): LoginAction => ({
  type: LOGIN,
  authentication,
});

export const logout = (authentication: boolean): LogoutAction => ({
  type: LOGOUT,
  authentication,
});

export const addUser = (user: string): AddUserAction => ({
  type: ADD_USER,
  user,
});

export const requestLogin = (
  user: string,
  password: string,
  history: any,
  setLoading: any
) => (dispatch: any) => {
  setLoading(true);
  apiHelper("/authenticate", "POST", { user, password })
    .then((res) => res.json())
    .then(({ auth_token, user }) => {
      if (auth_token) {
        dispatch(login(auth_token));
        dispatch(addUser({ ...user }));
        history.push("/profile");
        setLoading(false);
        return notification.success({
          message: "Login realizado",
        });
      } else {
        setLoading(false);
        return notification.error({
          message: "Login inválido",
          description:
            "Verifique os campos de usuário e senha e tente novamente",
        });
      }
    });
};
