import { ADD_SEARCH, AddSearchAction } from "./actionTypes";

export const addSearch = (search: any): AddSearchAction => ({ type: ADD_SEARCH, search });
