import { 
  GET_BOOKMARKS, 
  GET_USERS, 
  ADD_BOOKMARK, 
  DELETE_BOOKMARK, 
  GetBookmarksAction,
  GetUsersAction,
  AddBookmarkAction,
  DeleteBookmarkAction
} from "./actionTypes";

export const getBookmarks = (bookmarks: any): GetBookmarksAction => ({
  type: GET_BOOKMARKS,
  bookmarks,
});
export const getUsers = (users: string): GetUsersAction => ({
  type: GET_USERS,
  users,
});
export const addBookmark = (bookmark: any): AddBookmarkAction => ({
  type: ADD_BOOKMARK,
  bookmark,
});
export const deleteBookmark = (user: string): DeleteBookmarkAction => ({
  type: DELETE_BOOKMARK,
  user,
});



