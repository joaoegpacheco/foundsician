import { Form, Input, Button, Select } from "antd";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

import { profiles, cities } from "../../data";
import { errorRules } from "../../pages/RegisterUser/validator";
import {
  tailFormItemLayout,
  formItemLayout,
} from "../../styles/RegisterUser/RegisterUser";

const { Option } = Select;
const UserForm = ({
  isEdit,
  cellphone,
  name,
  onFinish,
  address,
  loading,
  about,
  image_url,
  profile,
  facebook_url,
  instagram_url,
}) => {
  const [disabled, setDisabled] = useState(isEdit);
  return (
    <Container>
      <Form
        style={{ paddingTop: "80px", width: "100%" }}
        {...formItemLayout}
        name="register"
        onFinish={onFinish}
        initialValues={{
          prefix: "55",
          name,
          cellphone,
          about,
          facebookUrl: facebook_url,
          instagramUrl: instagram_url,
          cidade: address,
          profile: profiles[profile],
          imageUrl: image_url,
        }}
        scrollToFirstError
      >
        <Form.Item
          name="name"
          label="Nome Completo"
          rules={!isEdit && errorRules.name}
        >
          <Input disabled={disabled} />
        </Form.Item>
        <Form.Item
          name="profile"
          label="Quem é você?"
          rules={!isEdit && errorRules.profile}
        >
          <Select disabled={disabled} placeholder="Selecione o que você faz">
            {Object.entries(profiles).map((profile, key) => (
              <Option key={key} value={profile[0]}>
                {profile[1]}
              </Option>
            ))}
            <Option value="10">Cliente</Option>
          </Select>
        </Form.Item>

        <Form.Item
          name="cidade"
          label="Cidade "
          hasFeedback
          rules={
            !isEdit && [
              { required: true, message: "Por favor, seleciona sua cidade!" },
            ]
          }
        >
          <Select disabled={disabled} placeholder="Seleciona sua cidade">
            {cities.map((city, key) => (
              <Option key={key} value={city.value}>
                {city.value}
              </Option>
            ))}
          </Select>
        </Form.Item>
        {!isEdit && (
          <Form.Item
            name="email"
            label="E-mail"
            rules={!isEdit && errorRules.email}
          >
            <Input disabled={disabled} />
          </Form.Item>
        )}

        <Form.Item
          name="cellphone"
          label="Telefone"
          rules={!isEdit && errorRules.cellphone}
        >
          <Input
            disabled={disabled}
            addonBefore="+55"
            style={{ width: "100%" }}
          />
        </Form.Item>
        {isEdit && (
          <>
            <Form.Item name="facebookUrl" label="Facebook">
              <Input disabled={disabled} />
            </Form.Item>
            <Form.Item name="instagramUrl" label="Instagram">
              <Input disabled={disabled} />
            </Form.Item>
            <Form.Item name="imageUrl" label="Foto de perfil">
              <Input disabled={disabled} />
            </Form.Item>
            <Form.Item name="about" label="Sobre você">
              <Input.TextArea disabled={disabled} />
            </Form.Item>
          </>
        )}
        {!isEdit && (
          <>
            <Form.Item
              name="password"
              label="Senha"
              rules={errorRules.registerpassword}
              hasFeedback
            >
              <Input.Password disabled={disabled} />
            </Form.Item>

            <Form.Item
              name="password_confirmation"
              label="Confirme sua senha"
              dependencies={["password"]}
              hasFeedback
              rules={errorRules.passwordconfirm}
            >
              <Input.Password disabled={disabled} />
            </Form.Item>
          </>
        )}
        <Form.Item {...tailFormItemLayout}>
          {!disabled && (
            <Button
              type="primary"
              htmlType="submit"
              loading={loading}
              style={{ ...styleSubmitForm }}
            >
              {isEdit ? "Salvar" : "Cadastrar novo usuário"}
            </Button>
          )}
          {!isEdit && <Link to="/login">Já tem uma conta? </Link>}
          {isEdit && disabled && (
            <Button
              type="primary"
              onClick={() => setDisabled(false)}
              loading={loading}
              style={{ ...styleSubmitEditForm }}
            >
              Editar
            </Button>
          )}
        </Form.Item>
      </Form>
    </Container>
  );
};

export default UserForm;

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const styleSubmitForm = {
  width: "480px",
  height: "40px",
  borderRadius: "4px",
  outline: "none",
  fontSize: "15px",
  boxSizing: "border-box",
};

const styleSubmitEditForm = {
  width: "415px",
  height: "40px",
  borderRadius: "4px",
  outline: "none",
  fontSize: "15px",
  boxSizing: "border-box",
};
