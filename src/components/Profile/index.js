import React from "react";

import "antd/dist/antd.css";
import "../../styles/UserProfile/UserProfile.css";

import { Layout, PageHeader, Typography } from "antd";

import facebook from "../../assets/img/facebook.png";
import instagram from "../../assets/img/instagram.png";
import { profiles } from "../../data";

const { Content } = Layout;
const { Paragraph } = Typography;

const IconLink = ({ src, text, href }) => (
  <a
    className="example-link"
    href={href}
    target="_blank"
    without
    rel="noopener noreferrer"
  >
    <img className="example-link-icon" src={src} alt={text} />
  </a>
);

const Profile = ({
  profile,
  name,
  imageUrl,
  facebookUrl,
  about,
  instagramUrl,
}) => {
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Layout className="site-layout">
        <Content style={{ margin: "0 16px" }}>
          <PageHeader
            title={name}
            className="site-page-header"
            subTitle={profiles[profile]}
            avatar={{ src: imageUrl }}
          >
            <Paragraph>{about}</Paragraph>
            <div>
              <IconLink
                src={facebook}
                style={{ width: 25 }}
                href={facebookUrl}
              />
              <IconLink
                src={instagram}
                style={{ width: 25 }}
                href={instagramUrl}
              />
            </div>
          </PageHeader>
        </Content>
      </Layout>
    </Layout>
  );
};
export default Profile;
