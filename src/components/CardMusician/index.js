import { UserOutlined, StarFilled } from "@ant-design/icons";
import { Card, notification } from "antd";
import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

import { addBookmark, deleteBookmark } from "../../actions/actionBookmarks";
import apiHelper from "../../apiHelper/apiHelper";
import avatar from "../../assets/img/default-avatar.png";
import { profiles } from "../../data";
import LinkTo from "../../components/Link";

const { Meta } = Card;

const CardMusician = ({
  user,
  authentication,
  bookmarks,
  addBookmark,
  deleteBookmark,
}) => {
  const handleBookMark = (user) => {
    const data = {
      book_mark: {
        marked_user_id: user.id,
      },
    };
    const bookmarkToDelete = bookmarks.find(
      (e) => e.marked_user.id === user.id
    );

    if (bookmarkToDelete) {
      deleteBookmark(user);

      apiHelper(
        `/book_marks/${bookmarkToDelete.id}`,
        "DELETE",
        null,
        authentication
      ).then(
        notification.success(
          {
            message: "Músico removido do favorito!",
          },
          2.5
        )
      );
    } else {
      addBookmark(user);

      apiHelper("/book_marks", "POST", data, authentication)
        .then((res) => res.json())
        .then(
          notification.success(
            {
              message: "Músico adicionado ao favorito!",
            },
            2.5
          )
        );
    }
  };

  const colorStar = (bookmarks, user) => {
    if (bookmarks.find((e) => e.marked_user.id === user.id)) {
      return "yellow";
    } else {
      return "";
    }
  };
  const { name, image_url, id, profile } = user;
  return (
    <ContainerCard>
      <Card
        style={{ width: 240 }}
        cover={<img style={{width: 240, height: 240 }} alt={name} src={image_url === null ? avatar : image_url} />}
        actions={[
          <LinkTo to={`/profile/${id}`}>
            <UserOutlined />
          </LinkTo>,
          <StarFilled
            style={{ color: colorStar(bookmarks, user) }}
            onClick={() => handleBookMark(user)}
          />,
        ]}
      >
        <Meta title={name} description={profiles[profile]} />
      </Card>
    </ContainerCard>
  );
};

const mapStateToProps = (state) => ({
  authentication: state.login.authentication,
  bookmarks: state.bookmarks.bookmarks,
});

const mapDispatchToProps = (dispatch) => ({
  addBookmark: (user) => dispatch(addBookmark({ id: 0, marked_user: user })),
  deleteBookmark: (user) =>
    dispatch(deleteBookmark({ id: 0, marked_user: user })),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CardMusician));

const ContainerCard = styled.div`
  height: 400px;
  width: 244px;
  margin: 20px;
`;
