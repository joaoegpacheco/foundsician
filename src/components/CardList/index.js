import React from 'react';
import styled from "styled-components";
import CardMusician from "../CardMusician";

const CardList = (props) => {
  return (
    <MainContainer>
      {props.list.map((musician, key) => {
      return (
          <CardMusician key={key} user={musician} />
      );
    })}
    </MainContainer>
  )
}

export default CardList;

const MainContainer = styled.div`
width: 1250px;
display: flex;
flex-wrap: wrap;
`