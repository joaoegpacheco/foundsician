import { SearchOutlined, UserOutlined, StarOutlined } from "@ant-design/icons";
import { Layout, Menu } from "antd";
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { logout } from "../../actions/actionLogin";

const { SubMenu } = Menu;
const { Sider } = Layout;

class SliderBar extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  };

  render() {
    return (
      <>
        <Sider
          collapsible
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <Menu theme="dark" mode="inline">
            <Menu.Item key="1" icon={<SearchOutlined />}>
              <Link to="/search">Buscar Profissionais</Link>
            </Menu.Item>
            <SubMenu key="sub1" icon={<UserOutlined />} title="Minha Conta">
              <Link to="/profile" />
              <Menu.Item key="3">
                <Link to="/profile">Perfil</Link>
              </Menu.Item>
              <Menu.Item key="4">
                <Link to="/editProfile">Alterar Cadastro</Link>
              </Menu.Item>
              <Menu.Item>
                <Link to="/login" onClick={this.props.logout} /> Sair
              </Menu.Item>
            </SubMenu>
            <Menu.Item key="2" icon={<StarOutlined />}>
              <Link to="/favoritos">Favoritos</Link>
            </Menu.Item>
          </Menu>
        </Sider>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  logout: (authentication) => dispatch(logout(authentication)),
});

export default connect(null, mapDispatchToProps)(SliderBar);
