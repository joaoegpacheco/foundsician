import { Layout, Menu } from "antd";
import React from "react";
import { Link } from "react-router-dom";

const { Item } = Menu;
const { Header } = Layout;

class TopBar extends React.Component {
  state = {
    collapsed: false,
  };

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <>
        <Layout>
          <Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
            <Menu
              theme="dark"
              mode="horizontal"
              style={{
                cursor: "pointer",
              }}
              inlineCollapsed={this.state.collapsed}
            >
              <Item key="1">
                <Link to="/register">Cadastre-se</Link>
              </Item>
              <Item key="2">
                <Link to="/login">Fazer Login</Link>
              </Item>
            </Menu>
          </Header>
        </Layout>
      </>
    );
  }
}

export default TopBar;
