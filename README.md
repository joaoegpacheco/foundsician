Qualquer implementação/correções serão feitas em branchs específicas para aquilo

- **fix/nome-da-alteração**
- **feature/nome-da-feature**

Vamos utilizar muito bem as pastas em nossos projetos e manter a organização

- **assets/** Para arquivos como imagens, fontes de texto, áudios...
- **pages/** Para páginas completas onde não temos componentes
- **components/** Para os demais componentes
- **components/nome-do-componente** Para aquele componente específico

  Exemplo: src/components/LoginPage/LoginPage.js

**Members**

**Product Owner**
Rafael Scherer

**Tech Leader**
Mauricio Crecencio

**Scrum Master**
João Pacheco

**Quality Assurance**
Raphael Carneiro
